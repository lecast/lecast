# LeCast
LeCast is a podcast feed self-hoster which accepts videos as input. The motivation and main usecase is hearing univeristy lectures which were recorded.

A freely hosted version can be found under c109-214.cloud.gwdg.de:8080/. It might disappear at any time and for any duration and comes without any guarantees.

## Design principles
Since lecture recordings usually contain lecturers and/or students LeCast is anonymous and private by default. 
All podcast feeds have randomly generated
There are no users, accounts,

## Typical usage scenario
Create a podcast list, register it in some podcatcher (e.g. podcast addict for android) and then:
1. Get hold on a lecture video(s)
2. Submit the video file to LeCast
3. wait for conversion beeing done
4. check your podcatcher or wait for it to notify you

## Dependencies 
You should have installed `node`, `yarn` and `ffmpeg`. 
It seams that you still get very old versions of `node` from some package distributors (looking at you, Canonical). 
Your version needs to support the fs-promises API which seams to be the case [with Versions >= 10](https://nodejs.org/dist/latest-v10.x/docs/api/fs.html#fs_fs_promises_api).
If you are not using a standard linux, you might need to update ffmpeg paths in [src/index.js](src/index.js).
Feel free to use `npm` instead of `yarn`, be warned that we didn't test it though.

## How to use
After installing all dependencies:
1. clone this repo
2. run `yarn` inside the repo
3. run `yarn run build` to build the frontend
4. run `yarn run start` to start the backend (which then serves the frontend)

## How to develop
We use [parcel](https://parceljs.org/) as bundler because we are lazy.
There is `yarn run frontend` which stats parcel bundler as development server.
You could also use `nodemon` for the backend but watch out for it reloading whenever new videos are uploaded.
Also we currently use vue in dev mode because we are lazy which is also why we do bug-fixes in production (yeah I know but as I said: no guarantees, this is a personal project).

## License
Sourcecode you get from the repo is MIT-licensed.
Contributions from external parties e.g. merge requests stand under CC0/public domain.
Feel free to fork it and/or send in merge requests.