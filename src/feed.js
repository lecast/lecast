const fs = require("fs");
const RSS = require("rss-generator");

// {
//     title: `${req.query.id}`,
//         feed_url: `${req.protocol}://${req.hostname}:${PORT}/${req.query.id}/rss.xml`,
//     site_url: `${req.protocol}://${req.hostname}:${PORT}/${req.query.id}/`
// }

async function createFeed(id, metadata) {
    let feeddata = {
        metadata,
        items: []
    };
    return await writeFeed(id, feeddata);
}

async function addtoFeed(id, item) {
    let feeddata = JSON.parse((await fs.promises.readFile(`${__dirname}/../public/${id}/feed.json`)).toString());
    // add item to feed
    feeddata.items.push(item);
    // update feed files
    return await writeFeed(id, feeddata);
}

function feed2xml(feeddata) {
    const feed = new RSS(feeddata.metadata);
    for (let i = 0; i < feeddata.items.length; i++) {
        feed.item(feeddata.items[i]);
    }
    return feed.xml();
}

async function writeFeed(id, feeddata) {
    // update rss and json
    const p1 = fs.promises.writeFile(`${__dirname}/../public/${id}/rss.xml`, feed2xml(feeddata));
    const p2 = fs.promises.writeFile(`${__dirname}/../public/${id}/feed.json`, JSON.stringify(feeddata));
    return await Promise.all([p1, p2]);
}

module.exports = {
    createFeed,
    addtoFeed
};