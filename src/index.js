const express = require("express");
const ffmpeg = require("fluent-ffmpeg");
const bodyParser = require("body-parser");
const fs = require("fs");
const fileUpload = require("express-fileupload");
const app = express();
const feed = require("./feed");
const crypto = require("crypto");
const asyncHandler = require("express-async-handler");
var cors = require("cors");

app.use(cors());

const PORT = 8080;
const home = `${__dirname}/../public`;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

// parse application/json
app.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
app.use(
    fileUpload({
        useTempFiles: true,
        tempFileDir: `${__dirname}/../tmp/`,
    })
);

ffmpeg.setFfmpegPath("/usr/bin/ffmpeg");
ffmpeg.setFfprobePath("/usr/bin/ffprobe");
ffmpeg.setFlvtoolPath("/usr/bin/ffplay");

app.get("/api", asyncHandler(async (req, res) => {
    res.send("up and running");
}));

function existsAsync(path) {
    return new Promise(function (resolve, reject) {
        fs.exists(path, function (exists) {
            resolve(exists);
        });
    });
}

// add new podcast
app.post("/api", asyncHandler(async (req, res) => {
    console.log(req.body);
    /*
    req.body should have format {
        title: "Alpaka",
        description: "In diesem Podcast geht es um das niedlichste Tier der Welt, den Breitmaulfrosch. Nein - Scherz, natürlich geht es hier um Alpakas, Alpapakas, Allpakas, Aalpakas und Co. (https://islieb.tumblr.com/image/182294993711)",
        id: "alpaka"
        thumbnail: null,
    }
     */
    const id = req.body.id;
    const feedDirectory = `${__dirname}/../public/${id}`;
    if (!await existsAsync(feedDirectory)) {
        await fs.promises.mkdir(feedDirectory);
        await feed.createFeed(id, {
            title: `${req.body.title}`,
            description: `${req.body.description}`,
            feed_url: `${req.protocol}://${req.hostname}:${PORT}/p/${id}/rss.xml`,
            site_url: `${req.protocol}://${req.hostname}:${PORT}/p/${id}/rss.xml`,
            image_url: `https://i.imgur.com/E4GjQTv.jpg`
        });
    }
    res.send(id);
}));
// info about one podcast
app.get("/api/:id", asyncHandler(async (req, res) => {
    // res.send({
    //     name: "Alpaka",
    //     description: "In diesem Podcast geht es um das niedlichste Tier der Welt, den Breitmaulfrosch. Nein - Scherz, natürlich geht es hier um Alpakas, Alpapakas, Allpakas, Aalpakas und Co. (https://islieb.tumblr.com/image/182294993711)",
    //     thumbnail: null,
    //     items: {
    //         name: "",
    //         description: "",
    //         filename: "",
    //         thumbnail: null
    //     }
    // });
    res.send(JSON.parse((await fs.promises.readFile(`${__dirname}/../public/${req.params.id}/feed.json`)).toString()));
}));

function convertToMP3(id, sourcefile) {
    return new Promise(function (resolve, reject) {
        console.log("starting to convert");
        // replace file extension with mp3
        let fileName = `${crypto.randomBytes(20).toString("hex")}.mp3`;
        // convert to mp3
        ffmpeg(sourcefile)
            .noVideo()
            .audioCodec("libmp3lame")
            .saveToFile(`${__dirname}/../public/${id}/${fileName}`)
            .on("end", function (stdout, stderr) {
                console.log("Finished converting");
                resolve(fileName);
                // remove video file
                fs.promises.unlink(sourcefile);
            })
            .on("error", function (err) {
                reject(err);
                // remove video file
                fs.promises.unlink(sourcefile);
            });
    });
}

const axios = require("axios");

// add new item to podcast
app.post("/api/:id", asyncHandler(async (req, res) => {
    console.log(req.body);
    console.log(req.files);
    /*
    req.body should have format {
        name: "",
        description: "",
        thumbnail: null
    }
     */
    const tmpfilename = crypto.randomBytes(20).toString("hex");
    let sourcefile = `${__dirname}/../tmp/${tmpfilename}`;
    const id = req.params.id;
    if (req.files && req.files.file) {
        // save video file to tmp directory
        req.files.file.mv(sourcefile, (err) => {});
    } else if (req.body.url) {
        console.log(`setting sourcefile path to ${sourcefile}`);
        let stream = (await axios({
            method: "get",
            url: req.body.url,
            responseType: "stream"
        })).data;
        let destfilestream = fs.createWriteStream(sourcefile);
        stream.pipe(destfilestream);
        console.log(await new Promise(function (resolve, reject) {
                destfilestream.on("end", () => resolve(1));
                // TODO: this is not good and should not be done. but the resolve(1) doesn't seam to be executed and this works
                stream.on("end", () => resolve(2) /*, destfilestream.close()*/);
                stream.on("error", (err) => {
                    reject(err);
                    try {
                        destfilestream.close();
                    } catch (e) {
                    }
                });
            }
        ));
    } else {
        return res.sendStatus(400);
    }

    let fileName = await convertToMP3(id, sourcefile);
// add converted file to feed
    await feed.addtoFeed(id, {
        title: req.body.name,
        description: req.body.description,
        enclosure: {url: `${req.protocol}://${req.hostname}:${PORT}/p/${id}/${fileName}`}
    });
    res.sendStatus(200);
}))
;

app.use("/", express.static(`${__dirname}/../dist`)); // all files except root should be served statically
app.use("/p", express.static(`${__dirname}/../public`)); // all files except root should be served statically

app.listen(PORT);
