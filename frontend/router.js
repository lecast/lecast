import Vue from "vue";
import VueRouter from "vue-router";
import About from "./components/About";
import CreatePodcast from "./components/AddPodcast";
import PodcastHome from "./components/PodcastHome";
import PodcastList from "./components/Home";
import Uploader from "./components/Uploader";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: "hash",
    linkActiveClass: "active",
    routes: [
        {path: "/", component: PodcastList},
        {path: "/about", component: About},
        {path: "/add", component: CreatePodcast},
        {path: "/:id", component: PodcastHome},
        {path: "/:id/add", component: Uploader}
    ]
});

export default router;