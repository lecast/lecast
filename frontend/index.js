import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import '../node_modules/vuetify/dist/vuetify.min.css'
import "@mdi/font/css/materialdesignicons.css";
import store from "./store";
import router from "./router";
import layout from "./components/Layout";

Vue.config.productionTip = false;
Vue.use(Vuetify);

import dark from './theme'


const vuetify = new Vuetify({
    theme: {
        themes: {
            dark,
            light: dark
        },
    },
})

new Vue({
    el: "#app",
    store,
    router,
    vuetify,
    render: h => h(layout),
    created () {
        this.$vuetify.theme.dark = true;
        window.vuetify = this.$vuetify
    },
});
