import Vuex from "vuex";
import Vue from "vue";
import axios from "axios";

Vue.use(Vuex);

let knownPodcasts = localStorage.getItem('knownPodcasts');
if(knownPodcasts === null){
    localStorage.setItem('knownPodcasts', '[]');
    knownPodcasts = [];
} else {
    knownPodcasts = JSON.parse(knownPodcasts);
}


export default new Vuex.Store({
    state: {
        knownPodcasts: knownPodcasts,
        x: 10,
        y: 5
    },
    getters: {
    },
    mutations: {
        addPodcast(state, id) {
            state.knownPodcasts.push(id);
            localStorage.setItem('knownPodcasts', JSON.stringify(state.knownPodcasts));
        }
    }
});