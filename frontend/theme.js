import colors from "../node_modules/vuetify/lib/util/colors";

export default {
    primary: '#ff9800',
    secondary: '#ff5722',
    accent: '#ffeb3b',
    error: '#e91e63',
    warning: '#f44336',
    info: '#673ab7',
    success: '#4caf50'
};
